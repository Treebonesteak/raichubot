import discord
from discord.ext import commands
import random
import os
import asyncio



class playing:
    def __init__(self, client):
        self.client = client
        self.statusPlaying = False
        self.dir_path = os.path.dirname(__file__).replace("/cogs", "/data") + "/playing.txt"

        """
        #Automatically start random playing-list
        if os.path.isfile(self.dir_path) and not os.stat(self.dir_path).st_size == 0:
            self.statusPlaying = True
            await self.playing_background()
        else:
            self.client.say("No playing status found!")
        """

    def __unload(self):
        #When cog unloads stop random shuffle if enabled
        self.statusPlaying = False

    #Set playing status
    @commands.command(pass_context=True, aliases=["set_playing"])
    async def setplaying(self, ctx, *args):
        """
        Sets the Playing Status of the bot or resets it.

        ?setplaying <Game>

        If <Game> is left empty, then the Playing Status gets reset.
        This command also stops the bot from randomly selecting a game, if enabled.
        """
        if not args:
            self.statusPlaying = False
            await self.client.change_presence(game=None)
        else:
            context = " ".join(args)
            self.statusPlaying = False
            await self.client.change_presence(game=discord.Game(name=context))
            await self.client.say("Changed playing status to: " + context)

    #Add playing game to file
    @commands.command(pass_context=True, aliases=["add_playing"])
    async def addplaying(self, ctx, *args):
        """
        Adds a game to the random rotation of the Playing Status of the bot.

        ?addplaying <Game>
        """
        if not args:
            return
        else:
            newGame = " ".join(args)
            if os.path.isfile(self.dir_path):
                #Appending to file
                with open(self.dir_path, "a", newline="", encoding="utf-8") as f:
                    f.write("\n" + newGame)
            else:
                #Creating and writing new file
                with open(self.dir_path, "x", newline="", encoding="utf-8") as f:
                    f.write(newGame)
            await self.client.say("Added \"" + str(newGame) + "\" to the list!")

    #Set random playing status
    @commands.command(pass_context=True, aliases=["random_playing"])
    async def randomplaying(self, ctx, *args):
        """
        Sets the Playing Status of the bot to randomly select from a list of Games.

        You can add more Playing Status to cycle through with the addplaying command.
        """
        if os.path.isfile(self.dir_path) and not os.stat(self.dir_path).st_size == 0:
            self.statusPlaying = True
            await self.client.say("Shuffeling playing status!")
            await self.playing_background()
            
        else:
            self.client.say("No playing status found!")

    #Background Task
    async def playing_background(self):
        await self.client.wait_until_ready()
        while not self.client.is_closed and self.statusPlaying == True:
            if os.path.isfile(self.dir_path) and not os.stat(self.dir_path).st_size == 0:
                responseArray = []
                
                #Reading out file
                with open(self.dir_path, newline="", encoding="utf-8") as f:
                    for row in f:
                        responseArray.append(row)

                #Shuffeling the list and giving output
                random.shuffle(responseArray)

                await self.client.change_presence(game=discord.Game(name=responseArray[0]))


            await asyncio.sleep(600)  #wait 600 seconds [10min] until doing it again

def setup(client):
    client.add_cog(playing(client))
