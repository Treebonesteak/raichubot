import discord
from discord.ext import commands
from random import randint


class dice:
    def __init__(self, client):
        self.client = client
        
    
    #Roll Fate Dice
    @commands.command(pass_context=True)
    async def rollfate(self, ctx):
        """
        Rolls a set of Fate™ Dice.

        A set of Fate™ Dice consists of 4 dice with the outcomes of +, - or 0
        """
        
        message = ctx.message
        await self.client.send_message(message.channel, rollFate())

    #Roll Dice
    @commands.command(pass_context=True)
    async def roll(self, ctx):
        """
        Lets you roll dice. You can even add or subract something too!

        ?roll <x>d<y>+<z>

        x ... # of dice
        y ... type of dice
        z ... # to add/subract

        e.g.
        ?roll 5d20-2
        """
        message = ctx.message
        print(str(message.content))
        print(str(message.author.name) + " used Roll")
        await self.client.send_message(message.channel, rollDice(message.content.upper().replace(ctx.prefix + "ROLL ", ""),True))

    #Roll Stats
    @commands.command(pass_context=True)
    async def rollstats(self, ctx):
        """
        Rolls 6 stats for a DnD Character. Rolls 4d6 drop lowest for each stat.

        At the bottom it gives you all of your stats plus the added total score of your roll.
        """
        message = ctx.message
        print(str(message.author.name) + " used RollStats")
        await self.client.send_message(message.channel, rollStats())

def rollFate():
    result = []
    finalResult = 0
    message = ""
    resultString = ""

    #Roll 4 Fate dice; Either minus, plus or Zero; and add them to the final Result
    for i in range(4):
        result.append(randint(-1,1))
        finalResult += result[i]
        if result[i] < 0:
            resultString += "-"
        elif result[i] == 0:
            resultString += "0"
        else:
            resultString += "+"
        resultString += " "


    message = "You rolled " + str(finalResult) + "\n" + "Your dice rolls were: " + resultString
    return message

def rollDice(diceInput, strOutput):
    diceInput = diceInput.replace(" ", "")
    diceInput = diceInput.upper()
    diceSep = "D"

    errorMessage = "Oh nowo! Something went tewwibwy wrong. Sowwy ;3;"
    #errorMessage = "Something went wrong! Looks like the input was false." +  "\n" + "Correct input looks like this: " + "\n" + "?roll 3d20-8"

    #Check if its a valid input
    if diceSep in diceInput:
        diceIndex = diceInput.index(diceSep)
    else:
        return errorMessage

    #Declaring Vars
    result = []
    finalResult = 0
    calcToDiceStr = ""
    calcToDice = 0

    def getDice(endTypeDice):
        try:
            numDice = ""
            typeDice = ""
            resultDiceRoll = []
            #Get the number of dice
            for x in range(diceIndex):
                numDice += diceInput[x]
            numDice = int(numDice)

            #Get the type of dice
            for y in range(diceIndex + 1, diceIndex + len(range(diceIndex + 1 ,endTypeDice + 1))):
                typeDice += diceInput[y]
            typeDice = int(typeDice)

            #Roll number of types of Dice
            for z in range(numDice):
                resultDiceRoll.append(randint(1, typeDice))

            return resultDiceRoll
        except:
            return errorMessage

    #Check for Math in Input
    if "+" in diceInput:
        diceAdd = diceInput.index("+")
        result = getDice(diceAdd)
        for r in range(diceAdd, len(diceInput)):
            calcToDiceStr += diceInput[r]
        #Check for Error
        try:
            calcToDice = int(calcToDiceStr)
        except:
            return errorMessage
    elif "-" in diceInput:
        diceAdd = diceInput.index("-")
        result = getDice(diceAdd)
        for r in range(diceAdd, len(diceInput)):
            calcToDiceStr += diceInput[r]
        #Check for Error
        try:
            calcToDice = int(calcToDiceStr)
        except:
            return errorMessage
    else:
        result = getDice(len(diceInput))

    #Check if there was an error
    if result == errorMessage:
        return errorMessage

    #Sort dice rolls and remove list syntax
    result.sort()
    result.reverse()
    resultStr = " ".join(str(e) for e in result)

    #Calculate final result
    for num in result:
        finalResult += num
    finalResult += calcToDice


    #Return a string or just raw numbers
    if strOutput == True:
        message = "You rolled " + str(finalResult) + "!" + "\n" + "Your dice rolls were: " + resultStr
        return message
    else:
        if calcToDice == 0:
            return result
        else:
            result.append(calcToDice)
            return result



def rollStats():
    #initialize Vars || Don't judge me for my naming scheme
    allStats = []
    rolls = []
    endMessage = ""
    rawFinalResult = []
    rawResultStr = []
    finalFinalResult = 0
    rawResults = []
    finalResult = []

    #Roll 6 4d6
    for x in range(6):
        #Append the lists so python won't shit itself
        rawFinalResult.append(0)
        rawResultStr.append("")
        rawResults.append([])
        #Actually roll the dice
        for num in rollDice("4d6",False):
            #Add actual Rolls for display later
            rawFinalResult[x] += num
            rawResultStr[x] += str(num) + " "

            rawResults[x].append(num)

        #Remove lowest roll and add up stats
        rawResults[x].pop()
        finalResult.append(0)
        for p in rawResults[x]:
            rolls.append(p)
            finalResult[x] += p

        #Display the actual rolls
        endMessage += "You rolled *" + str(rawFinalResult[x]) + "!*\n" + "Your rolls: " + rawResultStr[x] + "\n"

    #Add up all stats
    for y in rolls:
        finalFinalResult += y

    #Display the final stats, plus all stats summed up
    endMessage += "\n" + "**"+ ", ".join(str(e) for e in finalResult) + "**" + "\n"

    endMessage += "\n" + "Your stat rolls added up are: " + str(finalFinalResult)

    return endMessage




def setup(client):
    client.add_cog(dice(client))
