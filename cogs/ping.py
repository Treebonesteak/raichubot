import discord
from discord.ext import commands
import asyncio
import os
import random


class ping:
    def __init__(self, client):
        self.client = client

    #When Bot gets pinged
    async def pingbot(self, message):
        print(message.author.name + " pinged RaichuBot")
        #Set where to read them off from
        dir_path = os.path.dirname(__file__).replace("/cogs", "/data") + "/responses.txt"
        randomChance = random.randint(1, 10)
        toxicMessages = ["Don't @ me", u"\U0001F63E", "Mind your own buisness.", "I'm sorry Paul, I'm afraid I can't let you do that", "1v1 me", "PRIVATE I KNOW WE ARE IN COMBAT BUT GET DOWN AND GIVE ME 50 FOR THAT BIG PILE OF BULLSHIT", "Adje was pingst du"]

        print(dir_path)
        if os.path.isfile(dir_path):
            print("File found")
            responseArray = []

            #Reading out file
            with open(dir_path, newline="", encoding="utf-8") as f:
                for row in f:
                    responseArray.append(row)

            emoteList = message.server.emojis
            feelsGoodMan = ""
            for emote in emoteList:
                if emote.name == "feelsgoodman":
                    feelsGoodMan = str(emote)
                if emote.name == "monkaS":
                    responseArray.append(str(emote))
                
            

            #Shuffeling the list and giving output
            random.shuffle(responseArray)
            if ":feelsgoodman:" in message.content and u"\U0001F44B" in message.content:
                mes = str(u"\U0001F44B" + " " + str(feelsGoodMan) + " " + message.author.mention)
            elif "XD" in message.content.upper():
                mes = "xd"
            elif "OWO" in message.content.upper():
                mes = "OwO What's this?"
            elif "HELLO THERE" in message.content.upper():
                mes = "General Kenobi!"
            elif (message.author.id == "300703081266413570" or message.author.id == "166616089676087296") and randomChance == 1:
                mes = "UwU"
            #elif message.author.id == "300703081266413570":
            #    random.shuffle(toxicMessages)
            #    mes = toxicMessages[0]
            elif "THOUGHTS" in message.content.upper():
                if random.randint(1,2) == 1:
                    mes ="Yes!"
                else:
                    mes = "No!"
            elif responseArray[0] != None:
                mes = responseArray[0]

            mest = await self.client.send_message(message.channel, mes)
                
            print("RaichuBot was pinged by " + str(message.author.name) + "! Response: " + str(mest.content))


def setup(client):
    client.add_cog(ping(client))
