import discord
from discord.ext import commands
from discord.voice_client import VoiceClient
import asyncio


class reserve:
    def __init__(self, client):
        self.client = client


    #Reservieren
    @commands.command(pass_context=True)
    async def reserve(self, ctx):
        """
        Joins a given VC for up to 10mins with the users Nickname.

        e.g.
        ?reserve GeneralVC 4
        (Joins the VC \"GeneralVC\" for 4min)
        """
        message = ctx.message
        print(str(message.author.name) + " used Reserve")

        #Split Inputs
        inputReserve = message.content.split(" ")
        inputReserve.pop(0)

        #Set Timer
        waitCounterDefault = 5
        waitCounterMax = 15
        waitCounterInput = True
        if len(inputReserve) >= 2:
            try:
                waitCounter = int(inputReserve[-1])
                if waitCounter > waitCounterMax:
                    waitCounter = waitCounterMax
            except:
                waitCounter = waitCounterDefault
                waitCounterInput = False
        else:
            waitCounter = waitCounterDefault
            waitCounterInput = False

        #Set Channel Name with Spaces
        if len(inputReserve) > 1:
            if waitCounterInput:
                inputReserve.pop()
            exitChannelInput = " ".join(inputReserve)
            inputReserve = [exitChannelInput, waitCounter]

        #Check if the channel is on the server
        exitChannelId = inputReserve[0]
        announceChannel = False
        for chan in message.server.channels:
            if str(exitChannelId).upper() in chan.name.upper() and str(chan.type).upper() == "VOICE":
                announceChannel = True
                exitChannelId = chan

        #Set Reserved Nickname
        reservedNickname = "R: " + message.author.display_name
        if len(reservedNickname) > 32:
            for x in range(3):
                reservedNickname.pop()
                if len(reservedNickname) <= 32:
                    break
        
        
        #Run the other Co-Routine to count the time
        if not message.server.me.voice.voice_channel == None:
            await self.client.send_message(message.channel, "RaichuBot reserviert schon einen Platz für jemanden")
            await self.client.change_nickname(message.server.me, None)
        else:
            await self.client.send_message(message.channel, "Ein Platz in \"" + str(exitChannelId) + "\" wird reserviert für " + str(waitCounter) + "min!")
            await self.client.change_nickname(message.server.me, reservedNickname)
            self.client.loop.create_task(self.waitReserve(message.author, exitChannelId, waitCounter))


    async def waitReserve(self, author, channel, timer):
        #Join VC
        vc = await self.client.join_voice_channel(channel)

        timer *= 60
        waiting = 10
        for passedTime in range(int(timer/waiting)):
            await asyncio.sleep(waiting)
            if author.voice.voice_channel == vc.channel:
                break

        print("Leaving VC..")
        await self.client.change_nickname(channel.server.me, None)
        await vc.disconnect()



def setup(client):
    client.add_cog(reserve(client))
