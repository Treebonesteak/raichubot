import discord
from discord.ext import commands
import asyncio


class chat:
    def __init__(self, client):
        self.client = client

    
    #Announce
    @commands.command(pass_context=True)
    async def announce(self, ctx):
        """
        RaichuBot announces a given message to a given channel.

        ?announce <Channel> <Message>

        e.g.
        ?announce #general Free game on steam!
        (Bot will post \"Free game on steam!\" in the #general chat.)
        """
        message = ctx.message
        inputMessage = message.content.split(" ")
        exitMessage = ""
        announceChannel = False
        serverUsers = message.server.members
        atMeMessage = ""
        atMeTrue = True

        #Check if has enough strings
        if len(inputMessage) >= 3:
            #Put the exitMessage back together
            for s in range(2, len(inputMessage)):
                exitMessage += inputMessage[s] + " "

            #Check if the channel is on the server
            exitChannelId = inputMessage[1].replace("<#","").replace(">","")
            for chan in message.server.channels:
                if exitChannelId == chan.id:
                    announceChannel = True
                    exitChannelId = chan

            #Check if @ is in the message
            if "@" in exitMessage:
                atMe = exitMessage.index("@")
                for x in range(atMe + 1, len(exitMessage)):
                    if atMeTrue and exitMessage[x] != " ":
                        atMeMessage += exitMessage[x]
                    elif exitMessage[x] == " ":
                        atMeTrue = False

                #Check for @Name in User List and replace Message with ping
                for user in serverUsers:
                    if atMeMessage == user.name or atMeMessage == user.display_name or atMeMessage == user.id:
                        atMeMessage = "@" + atMeMessage
                        exitMessage = exitMessage.replace(atMeMessage, "<@!" + str(user.id) + ">")

            #Output Messages
            if announceChannel:
                print(str(message.author.name) + " announced in #" + str(exitChannelId) + ": " + str(exitMessage))
                await self.client.send_message(exitChannelId, exitMessage)
            else:
                await self.client.send_message(message.channel, "Input a channel to announce in!")
        else:
            await self.client.send_message(message.channel, "Something went wrong!")


    #Purge
    @commands.command(pass_context=True)
    async def purge(self, ctx):
        """
        Will deleted a number of messages. Optionally a user can be picked, so only the users messages get deleted.

        ?purge <#> <user>

        e.g.
        ?purge 10 @Treebonesteak
        (Bot will delete 10 of Treebonesteak's latest messages.)
        """
        message = ctx.message
        if message.author.permissions_in(message.channel).manage_messages:

            purge = message.content.split(" ")
            purge.pop(0)

            numMessages = 0
            beforeMessage = None
            afterMessage = None
            checkMessage = None
            errorMessage = "Pwease input good thwings next twime, I couldn't hewlp youz ;3;"

            #Check Input for what kind of action to perform
            if len(purge) >= 2 and await isInt(purge[0]):
                try:
                    numMessages = int(purge[0])
                    #Delete from purge[1]
                    #Number of messages to delete purge[0]

                    #Check if Name is on server
                    for user in message.server.members:
                        if purge[1] == user.id or purge[1].upper() == user.name.upper() or purge[1].upper() == user.display_name.upper() or purge[1] == user.mention:
                            checkMessage = user
                            break
                    else:
                        #if no User was found
                        checkMessage = errorMessage
                        await self.client.send_message(message.channel, errorMessage)
                except Exception as e:
                    print(e)
                    checkMessage = None
            elif len(purge) == 1:
                try:
                    numMessages = int(purge[0])
                except:
                    checkMessage = None
            else:
                await self.client.send_message(message.channel, errorMessage)


            #Handle the Deletion
            try:
                #Actually delete Messages
                if checkMessage != None and checkMessage != errorMessage:
                    actualMesDel = []
                    async for m in self.client.logs_from(message.channel, limit=100):
                        if m.author == checkMessage and numMessages > 0 and not (m.timestamp == message.timestamp and m.author == message.author):
                            numMessages -= 1
                            actualMesDel.append(m)
                    deleted = actualMesDel
                    await self.client.delete_messages(actualMesDel)
                elif checkMessage != errorMessage:
                    deleted = await self.client.purge_from(message.channel, limit=numMessages, check=checkMessage, before=message, after=afterMessage)
                else:
                    deleted = []
            except Exception as e:
                #Write the exeption and delete the messages after
                print("Error deleting messages; ", e)
                delmes = await self.client.send_message(message.channel, str(e))
                await asyncio.sleep(10)
                await self.client.delete_message(delmes)
                await self.client.delete_message(message)
            else:
                #Confirm deletion and delete messages after
                delmes = await self.client.send_message(message.channel, 'Deleted {} message(s)'.format(len(deleted)))
                await asyncio.sleep(10)
                await self.client.delete_message(delmes)
                await self.client.delete_message(message)
            
            
        else:
            await self.client.send_message(message.channel, "I am sorry Dave, I am afraid I can't let you do that.")


    
    #GentlePing
    @commands.command(pass_context=True)
    async def gentle(self, ctx):
        """
        Pings everyone with a specific role, if they are online.

        ?gentle <RoleName> <Message>

        <RoleName> doesn't have to be the full name of the role, it can be just a part of it. Also dont use @<RoleName> or you'll ping everyone regardless!
        <Message> is optional. It will add a Message to the gentle Ping.
        
        e.g.
        ?gentle Alpha Hey guys!
        (Writes \"Hey Guys\" and pings everyone with the role \"Alpha\" that is online.)
        """
        message = ctx.message
        #Split incomming message and remove command
        rolePing = message.content.split(" ")
        rolePing.pop(0)
        
        roleUsers = []

        #Look for role on server
        for role in message.server.roles:
            if str(rolePing[0]).upper() in role.name.upper() and not role.is_everyone:
                rolePingServer = role
                              
        if rolePingServer:
            #Check if role was found and then search users with that role that are online
            for user in message.server.members:
                if rolePingServer in user.roles and not user.bot and str(user.status) == "online":
                    roleUsers.append(user)
                    

            if roleUsers:
                #Check if any users were found and turn @mentions of them to string
                roleUsersStr = ", ".join(u.mention for u in roleUsers)

                #Check if a message was added extra and display it
                if len(rolePing) > 1:
                    rolePing.pop(0)
                    mes = await self.client.send_message(message.channel, str(message.author.display_name) + " gently pings " + str(rolePingServer) + ":\n" + roleUsersStr + "\n\n" + " ".join(rolePing))
                else:
                    mes = await self.client.send_message(message.channel, str(message.author.display_name) + " gently pings " + str(rolePingServer) + ":\n" + roleUsersStr)
                await self.client.add_reaction(mes, discord.utils.get(message.server.emojis, name="AngryPingSock"))
            else:
                await self.client.send_message(message.channel, "Noone with that role online.")
        else:
            await self.client.send_message(message.channel, "Couldn't find the role you were looking for.")



async def isInt(s):
    #See if String is an int
    try: 
        int(s)
        return True
    except:
        return False


def setup(client):
    client.add_cog(chat(client))
