import discord
from discord.ext import commands
import csv
from random import randint
import os

seperator = "|"

class quote:
    def __init__(self, client):
        self.client = client
            
    #QuoteRead
    @commands.command(pass_context=True, aliases=["q"])
    async def quote(self, ctx):
        """
        Displays a Quote of a given user.
        Optionally you can add a number after the user to get a specific quote.

        ?quote @<user> <#>

        e.g.
        ?quote @Treebonesteak 1
        (Displays quote #1 of Treebonesteak)
        """
        message = ctx.message
        print(str(message.author.name) + " used GetQuote")
        
        inputQuote = message.content.split(" ")
        userArray = message.server.members
        quoteId = None
        userArrayName = False
        quoteNumber = 0

        try:
            quoteNumber = int(inputQuote[2])
        except:
            quoteNumber = 0

        #Check if Name is on server
        for user in userArray:
            if inputQuote[1] in user.mention or inputQuote[1] in user.name or inputQuote[1] in user.display_name:
                userArrayName = True
                foundUser = user
                quoteId = user.id

        if userArrayName:
            dir_path = os.path.dirname(__file__).replace("/cogs", "/data") + "/Quotes_" + message.server.id + ".txt"


            #Set Embed Colour
            embed = discord.Embed(
                colour = discord.Colour.dark_gold()
            )
            try:
                embed.set_author(name=foundUser.name, icon_url=foundUser.avatar_url)
            except:
                embed.set_author(name=foundUser.name)
            embed.description = getQuote(dir_path, quoteId, quoteNumber)

            #Send Embed
            await self.client.send_message(message.channel, embed=embed)
        else:
            await self.client.send_message(message.channel, "Please input a valid user!")

    #QuoteWrite
    @commands.command(pass_context=True, aliases=["aq"])
    async def addquote(self, ctx):
        """
        Adds a Quote to a given user.

        ?addquote @<user> <Quote>

        e.g.
        ?quote @Treebonesteak My Waifu is Glucose intolerant
        (Adds \"My Waifu is Glucose intolerant\" to Treebonesteak's quotes)
        """
        message = ctx.message
        print(str(message.author.name) + " used AddQuote")
        
        inputQuote = message.content.split(" ")
        quoteId = None
        userArrayName = False
        quoteToAdd = ""

        #Check if Name is on server
        for user in message.server.members:
            if inputQuote[1] in user.mention or inputQuote[1] in user.name or inputQuote[1] in user.display_name:
                userArrayName = True
                foundUser = user
                quoteId = user.id

        #Get Quote from input
        try:
            inputQuote.pop(0)
            inputQuote.pop(0)
            for string in inputQuote:
                quoteToAdd += string + " "
            quoteToAdd = quoteToAdd[:-1]
        except:
            quoteToAdd = ""

        #When User exists and quote was added
        if userArrayName and quoteToAdd != "":
            dir_path = os.path.dirname(__file__).replace("/cogs", "/data") + "/Quotes_" + message.server.id + ".txt"


            #Set Embed Colour
            embed = discord.Embed(
                colour = discord.Colour.dark_gold()
            )
            try:
                embed.set_author(name=foundUser.name, icon_url=foundUser.avatar_url)
            except:
                embed.set_author(name=foundUser.name)
            embed.description = newQuote(dir_path, quoteId, quoteToAdd)

            #Send Embed
            await self.client.send_message(message.channel, embed=embed)
            
        #When User exists but *no* quote was added
        elif userArrayName and quoteToAdd == "":
            await self.client.send_message(message.channel, "Please add a quote!")
            
        #When no User was found with that name
        else:
            await self.client.send_message(message.channel, "Please input a valid user!")


    #QuoteDelete
    @commands.command(pass_context=True, aliases=["dq"])
    async def deletequote(self, ctx):
        """
        Deletes a Quote of a given user.

        ?deletequote @<user> <QuoteNumber>

        e.g.
        ?quote @Treebonesteak 5
        (Deletes #5 of Treebonesteak's quotes)
        """
        message = ctx.message
        print(str(message.author.name) + " used DeleteQuote")
        
        
        inputQuote = message.content.split(" ")
        userArray = message.server.members
        quoteId = inputQuote[1].replace("@", "").replace("<", "").replace(">", "").replace("!", "")
        userArrayName = False
        quoteNumber = 0
        time = 15

        try:
            quoteNumber = int(inputQuote[2])
        except:
            quoteNumber = 0

        #Check if Name is on server
        for user in userArray:
            if quoteId == user.id or inputQuote[1] == user.name or inputQuote[1] == user.display_name:
                userArrayName = True


        #Delete Quote
        if userArrayName and quoteNumber > 0:
            dir_path = os.path.dirname(__file__).replace("/cogs", "/data") + "/Quotes_" + message.server.id + ".txt" #SetFile Location

            if not getQuote(dir_path, quoteId, quoteNumber).startswith("Not a valid Quote number!"):
                #Give out actual Quote first
                mes = await self.client.send_message(message.channel, "You want to delete this quote?\n" + getQuote(dir_path, quoteId, quoteNumber))

                await self.client.add_reaction(mes, u"\U0001F44D") #Thumbs Up
                await self.client.add_reaction(mes, u"\U0001F44E") #Thumbs Down

                rea = await self.client.wait_for_reaction([u"\U0001F44D", u"\U0001F44E"], user=message.author, message=mes, timeout=time)
                #reaDown = await self.client.wait_for_reaction(u"\U0001F44E", user=message.author, message=mes, timeout=time)

                if rea == None:                           #If nothing was added
                    await self.client.delete_message(mes)
                elif rea.reaction.emoji == u"\U0001F44E": #If Thumbs Down
                    await self.client.delete_message(mes)
                elif rea.reaction.emoji == u"\U0001F44D": #If Thumbs Up
                    await self.client.send_message(message.channel, delQuote(dir_path, quoteId, quoteNumber))
                    await self.client.delete_message(mes)
                else:                                     #Just in case something weird is wrong with the if
                    await self.client.delete_message(mes)
                    
            else:
                await self.client.send_message(message.channel, getQuote(dir_path, quoteId, quoteNumber))

            await self.client.delete_message(message)
                
            """
            if not reaUp == None and reaDown == None: #If Thumbs Up
                await self.client.send_message(message.channel, delQuote(dir_path, quoteId, quoteNumber))
                await self.client.delete_message(mes)
            elif reaUp == None and not reaDown == None: #If Thumbs Down
                await self.client.delete_message(mes)
            elif reaUp == None and reaDown == None: #If no reaction
                await self.client.delete_message(mes)
            else:                                   #If both reaction
                await self.client.delete_message(mes)
            """
        else:
            await self.client.send_message(message.channel, "Please input a valid user!")






def getQuote(fileLocation, user, userNumberQuote):
    userFound = False
    userQuotes = []

    #Check if csv file exists
    if os.path.isfile(fileLocation):
        #
        #Read the file
        #
        with open(fileLocation, newline="", encoding="utf-8") as csvfile:
            csvReader = csv.reader(csvfile, delimiter=seperator, quotechar=";")
            for row in csvReader:
                if user == row[0]:
                    userFound = True
                    userQuotes = row

        #
        #Read out random or specific Quote from user
        #
        errorMessageRead = "No Quotes found!"
        if userNumberQuote == 0:
            #Read out a random Quote from user
            if userFound:
                rando = randint(1,len(userQuotes)-1)
                return "#" + str(rando) + ": " + userQuotes[rando]
            else:
                return errorMessageRead
        else:    
            #Read out a specific Quote form user
            if userFound:
                try:
                    return "#" + str(userNumberQuote) + ": " + userQuotes[userNumberQuote]
                except:
                    return "Not a valid Quote number! The user has " + str(len(userQuotes)-1) + " Quotes"
            else:
                return errorMessageRead
    else:
        return "Something went wrong with the file path"



def newQuote(fileLocation, user, userNewQuote):
    #Check if csv file exists
    if os.path.isfile(fileLocation):
        #
        #Read the file
        #
        userFound = False
        userQuotes = []
        userNumberQuote = 0
        with open(fileLocation, newline="", encoding="utf-8") as csvfile:
            csvReader = csv.reader(csvfile, delimiter=seperator, quotechar=";")
            for row in csvReader:
                if user == row[0]:
                    userFound = True
                    userQuotes = row

        #
        #Write new Quote
        #
        with open(fileLocation, "r+", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=seperator, quotechar=";")
            reader = csv.reader(csvfile, delimiter=seperator, quotechar=";")
            
            fileContent = []

            #Copy contents of the whole file and add the quote
            if userFound:
                for row in reader:
                    if user == row[0]:
                        row.append(userNewQuote)
                        fileContent.append(row)
                    else:
                        fileContent.append(row)
                userNumberQuote = len(userQuotes)
            else:
                for row in reader:
                    fileContent.append(row)
                fileContent.append([user,userNewQuote])
                userNumberQuote = 1

            #Delete old file and write in old file + new quote
            csvfile.seek(0)
            csvfile.truncate()
            for row in fileContent:
                writer.writerow(row)

        return "Quote #" + str(userNumberQuote) + " added: " + "<@!" + user + ">: "+ userNewQuote
    else:
        with open(fileLocation, "w", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=seperator, quotechar=";")
            writer.writeline([user, userNewQuote])
        return "Quote #1 added: " + userNewQuote + "\n I also created a new file for you!"


def delQuote(fileLocation, user, userNumberQuote):
    userFound = False
    userQuotes = []

    #Check if csv file exists
    if os.path.isfile(fileLocation):
        #
        #Read the file
        #
        with open(fileLocation, newline="", encoding="utf-8") as csvfile:
            csvReader = csv.reader(csvfile, delimiter=seperator, quotechar=";")
            for row in csvReader:
                if user == row[0]:
                    userFound = True
                    userQuotes = row

        #
        #Read out random or specific Quote from user
        #
        errorMessageRead = "No Quotes found!"
        if userNumberQuote == 0:
            #Handle if no quote number was given
            return "No specific quote was given!"
        else:    
            #Read out a specific Quote form user
            if userFound:
                if userNumberQuote <= (len(userQuotes)-1) and userNumberQuote > 0: #Check if the Quote exists
                    deletedQuote = userQuotes.pop(userNumberQuote) #Delete the Quote from list

                    #
                    #Write back to the file
                    #
                    with open(fileLocation, "r+", newline="") as csvfile:
                        writer = csv.writer(csvfile, delimiter=seperator, quotechar=";")
                        reader = csv.reader(csvfile, delimiter=seperator, quotechar=";")
                        
                        fileContent = []

                        #Copy contents of the whole file and replace the row
                        for row in reader:
                            if user == row[0]:
                                fileContent.append(userQuotes)
                            else:
                                fileContent.append(row)

                        #Delete old file and write in old file + deleted quote
                        csvfile.seek(0)
                        csvfile.truncate()
                        for row in fileContent:
                            writer.writerow(row)

                    
                    return "Deleted Quote #" + str(userNumberQuote) + ": " + deletedQuote
                else:
                    return "Not a valid Quote number! The user has " + str(len(userQuotes)-1) + " Quotes"
            else:
                return errorMessageRead
    else:
        return "Something went wrong with the file path"


def setup(client):
    client.add_cog(quote(client))
