import discord
from discord.ext import commands
import asyncio
import os
import random
from collections import OrderedDict


class helper:
    def __init__(self, client):
        self.client = client
        self.prefix = self.client.prefix

    
    @commands.command(pass_context=True, hidden=True)
    async def oldhelp(self, ctx, *args):
        #Set Embed Colour
        embed = discord.Embed(
            colour = discord.Colour.dark_gold()
        )
        embed.set_author(name="Help", icon_url=self.client.user.avatar_url)

        #Set # of Commands per help page
        maxPerPage = 8


        #All Commands plus how to use ##Dont forget to add a  "," if adding a new command
        masterD = {
            "roll":{"name":"roll", "value":"Lets you roll dice", "usage":"Lets you roll dice. You can even add or subract something too! \n\n ?roll <x>d<y>+<z> \n\n x ... # of dice \n y ... type of dice \n z ... # to add/subract \n\n e.g. \n ?roll 5d20-2"},
            "rollfate":{"name":"rollFate", "value":"Rolls a set of Fate™ Dice", "usage":"Rolls a set of Fate™ Dice. \n A set of Fate™ Dice consists of 4 dice with the outcomes of +, - or 0"},
            "rollstats":{"name":"rollStats", "value":"Rolls 6 stats for a DnD Character", "usage":"Rolls 6 stats for a DnD Character. Rolls 4d6 drop lowest for each stat. \n At the bottom it gives you all of your stats plus the added total score of your roll."},
            "reserve":{"name":"reserve", "value":"Joins a given VC for up to 10mins with the users Nickname", "usage":"Joins a given VC for up to 10mins with the users Nickname. \n\n e.g.\n ?reserve GeneralVC 4 \n (Joins the VC \"GeneralVC\" for 4min)"},
            "gentle":{"name":"gentle", "value":"Pings everyone with a specific role, if they are online", "usage":"Pings everyone with a spme of the role, it can be just a part of it.\n <Message> is optional. It will add a Message to the gentle Ping. \n\ne.g.\n ?gentle Alpha Hey guys!\n (Writes \"Hey Guys\" and pings everyone with the role \"Alpha\" that is online.ecific role, if they are online. \n\n ?gentle <RoleName> <Message>\n\n <RoleName> doesn't have to be the full name of the role, it can be just a part of it.\n <Message> is optional. It will add a Message to the gentle Ping. \n\ne.g.\n ?gentle Alpha Hey guys!\n (Writes \"Hey Guys\" and pings everyone with the role \"Alpha\" that is online."},
            "quote":{"name":"quote", "value":"Displays a Quote of a given user", "usage":"Displays a Quote of a given user. \nOptionally you can add a number after the user to get a specific quote \n\n?quote @<user> <#>\n\n e.g.\n ?quote @Treebonesteak 1\n (Displays quote #1 of Treebonesteak)"},
            "addquote":{"name":"addquote", "value":"Adds a Quote to a given user", "usage":"Adds a Quote to a given user. \n\n ?addquote @<user> <Quote>\n\n e.g.\n ?quote @Treebonesteak My Waifu is Glucose intolerant\n (Adds \"My Waifu is Glucose intolerant\" to Treebonesteak's quotes)"},
            "deletequote":{"name":"deletequote", "value":"Deletes a Quote of a given user", "usage":"Deletes a Quote of a given user. \n\n ?deletequote @<user> <QuoteNumber>\n\n e.g.\n ?quote @Treebonesteak 5\n (Deletes #5 of Treebonesteak's quotes)"},
            "announce":{"name":"announce", "value":"RaichuBot announces a given message to a given channel", "usage":"RaichuBot announces a given message to a given channel. \n\n ?announce <Channel> <Message>\n\ne.g.\n?announce #general Free game on steam! \n(Bot will post \"Free game on steam\" in the #general chat."},
            "purge":{"name":"purge", "value":"Will deleted a number of messages", "usage":"Will deleted a number of messages. Optionally a user can be picked, so only the users messages get deleted \n\n ?purge <#> <user>\n\ne.g.\n?purge 10 @Treebonesteak! \n(Bot will delete 10 of Treebonesteak's latest messages."}
            }
        masterDict = OrderedDict(sorted(masterD.items()))

        #Calculate if there need to be more pages
        pages = 1

        if len(masterDict) > maxPerPage:
            if len(masterDict) % maxPerPage == 0:
                pages = int((len(masterDict)/maxPerPage))
            else:
                pages = int((len(masterDict)/maxPerPage) + 1)

        #If the help command withouth any arguments was triggered

        if not args:
            forCount = 0
            embed.set_footer(text="Page 1/" + str(pages))
            for key in masterDict:
                masterDict[key]["name"] = self.prefix + masterDict[key]["name"]
                embed.add_field(name=masterDict[key]["name"], value=masterDict[key]["value"], inline=False)
                forCount += 1
                if pages > 1 and forCount >= maxPerPage:
                    break

        #A specific command was requested
        elif args[0].lower() in masterDict:
            embed.add_field(name=self.prefix + masterDict[args[0].lower()]["name"], value=masterDict[args[0].lower()]["usage"])

        #A page number was requested
        elif await isInt(args[0]):
            if int(args[0]) >= 1 and int(args[0]) <= pages:
                #Page Number Exists
                forCount = int(-maxPerPage*(int(args[0])-1))
                embed.set_footer(text="Page " + args[0] + "/" + str(pages))

                for key in masterDict:
                    forCount += 1
                    if forCount > 0 and forCount <= maxPerPage:
                        masterDict[key]["name"] = self.prefix + masterDict[key]["name"]
                        embed.add_field(name=masterDict[key]["name"], value=masterDict[key]["value"], inline=False)

            else:
                embed.add_field(name="Error", value="Please input a valid page number!")
        else:
            embed.add_field(name="Error", value="Please input one of the commands or a page number!")
            

        await self.client.send_message(ctx.message.channel, embed=embed)

async def isInt(s):
    #See if String is an int
    try: 
        int(s)
        return True
    except:
        return False


def setup(client):
    client.add_cog(helper(client))
