import discord
from discord.ext import commands
import asyncio
import os
import random
from collections import OrderedDict


class nhelper:
    def __init__(self, client):
        self.client = client
        self.prefix = self.client.prefix


    @commands.command(pass_context=True, aliases=["h"])
    async def help(self, ctx, *args):
        """
        Lists help for all commands. Try ?help <command>

        ?help <command/page>

        Enter a command to get detailed help on how to use it
        or
        Enter a page number to cycle through the help command
        """
        # Store all commands in List and sort out duplicates, ignore command if hidden:True
        commandList = []
        for command in self.client.commands:
            for key in self.client.commands:
                if self.client.commands[command] == self.client.commands[key] and not self.client.commands[command] in commandList and not self.client.commands[command].hidden:
                    commandList.append(self.client.commands[command])

        """
        # Testing
        print(len(commandList))
        for c in range(len(commandList)):
            if commandList[c].name == "rollfate":
                print(commandList[c].name)
                print(commandList[c].help)
                commandList[c].brief = commandList[c].help.split("\n", 1)
                commandList[c].brief = commandList[c].brief.pop(0)
                print(commandList[c].brief)
                print(commandList[c].aliases)
                #print(commandList[0].__doc__)
        """

        # Put everything into a ordered dictionary
        Dict = {}
        for c in range(len(commandList)):
            # Read out docstring of command for help descripton
            try:
                tempHelp = commandList[c].help
                if "?" in tempHelp: tempHelp = tempHelp.replace("?", self.prefix)
            except:
                tempHelp = "No help for this command available!"

            # If there are any aliases for the command, append them at the end of the help
            if commandList[c].aliases:
                tempAli = []
                for ali in commandList[c].aliases:
                    tempAli.append(self.prefix + ali)
                tempHelp = tempHelp + "\n\nAliases for this command are:\n" + ", ".join(tempAli)

            # Take the first line of the help description as a brief summary
            try:
                tempBrief = tempHelp.split("\n",1)
                tempBrief = tempBrief.pop(0)
            except:
                tempBrief = tempHelp            

            # Join everything into a sorted dictionary
            tempD = {"name":commandList[c].name, "brief":tempBrief, "help":tempHelp}
            Dict[commandList[c].name] = tempD
        masterDict = OrderedDict(sorted(Dict.items()))



        # Set Embed Colour
        embed = discord.Embed(
            colour = discord.Colour.dark_gold()
        )
        embed.set_author(name="Help", icon_url=self.client.user.avatar_url)

        # Set # of Commands per help page
        maxPerPage = 8

        # Calculate if there need to be more pages
        pages = 1

        if len(masterDict) > maxPerPage:
            if len(masterDict) % maxPerPage == 0:
                pages = int((len(masterDict)/maxPerPage))
            else:
                pages = int((len(masterDict)/maxPerPage) + 1)


        # If the help command withouth any arguments was triggered
        if not args:
            forCount = 0
            embed.set_footer(text="Page 1/" + str(pages))
            for key in masterDict:
                masterDict[key]["name"] = self.prefix + masterDict[key]["name"]
                embed.add_field(name=masterDict[key]["name"], value=masterDict[key]["brief"], inline=False)
                forCount += 1
                if pages > 1 and forCount >= maxPerPage:
                    break

        # A specific command was requested
        elif args[0].lower() in masterDict:
            embed.add_field(name=self.prefix + masterDict[args[0].lower()]["name"], value=masterDict[args[0].lower()]["help"])

        # A page number was requested
        elif await isInt(args[0]):
            if int(args[0]) >= 1 and int(args[0]) <= pages:
                #Page Number Exists
                forCount = int(-maxPerPage*(int(args[0])-1))
                embed.set_footer(text="Page " + args[0] + "/" + str(pages))

                for key in masterDict:
                    forCount += 1
                    if forCount > 0 and forCount <= maxPerPage:
                        masterDict[key]["name"] = self.prefix + masterDict[key]["name"]
                        embed.add_field(name=masterDict[key]["name"], value=masterDict[key]["brief"], inline=False)

            else:
                embed.add_field(name="Error", value="Please input a valid page number!")
        else:
            embed.add_field(name="Error", value="Please input one of the commands or a page number!")
            

        await self.client.send_message(ctx.message.channel, embed=embed)


async def isInt(s):
    # See if String is an int
    try: 
        int(s)
        return True
    except:
        return False


def setup(client):
    client.add_cog(nhelper(client))
