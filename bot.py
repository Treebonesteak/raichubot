# RaichuBot by Treebonesteak
import logging
import asyncio
import os
import time
import discord
from discord.ext import commands
from discord.ext.commands import Bot


prefix = "?"
client = Bot(command_prefix=prefix, case_insensitive=True)
ClientE = discord.Client()

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

client.remove_command("help")
client.prefix = prefix
client.botCommanderId = "144861450995761152"


@client.event
async def on_ready():
    print("Ready when you are")
    print(client.user.name)
    print(client.user.id)
    await load_extension()
    print()



"""
EXTENTION COMMANDS
"""

@client.command(pass_context=True, aliases=["rex"], hidden=True)
async def reload_extensions(ctx):
    """Reloads the extensions [cogs] of the bot."""
    if ctx.message.author.id == client.botCommanderId:
        try:
            errorMessagesUnload = await unload_extension()
            errorMessagesLoad = await load_extension()
            print("RELOADED THE EXTENSIONS\n")
        except:
            await client.say("Something went wrong with reloading the extensions!")
        finally:
            if not errorMessagesUnload and not errorMessagesLoad:
                await client.say("Successfully reloaded the extensions!")
            else:
                await client.say("Error with the following extensions:")
                if errorMessagesUnload:
                    for e in errorMessagesUnload:
                        await client.say(e)
                if errorMessagesLoad:
                    for e in errorMessagesLoad:
                        await client.say(e)


#Load Extentions
async def load_extension():
    initial_extensions = []
    errorMessages = []
    for pyfile in os.listdir(os.path.dirname(os.path.realpath(__file__)) + "/cogs"):  #Get /cog directory and read out all files with .py
        if ".py" in pyfile:
            pyfilestr = str(pyfile).replace(".py", "")
            initial_extensions.append("cogs." + str(pyfilestr))
    for extension in initial_extensions:
        try:
            client.load_extension(extension)
        except Exception as e:
            print('Failed to load extension {0}'.format(extension))
            print("Exception: " + str(e))
            print("-")
            errorMessages.append(extension + ": " + str(e))
    return errorMessages

#Unload Extentions
async def unload_extension():
    initial_extensions = []
    errorMessages = []
    for pyfile in os.listdir(os.path.dirname(os.path.realpath(__file__)) + "/cogs"):  #Get /cog directory and read out all files with .py
        if ".py" in pyfile:
            pyfilestr = str(pyfile).replace(".py", "")
            initial_extensions.append("cogs." + str(pyfilestr))
    for extension in initial_extensions:
        try:
            client.unload_extension(extension)
        except Exception as e:
            print('Failed to unload extension {0}'.format(extension))
            print("Exception: " + str(e))
            print("-")
            errorMessages.append(extension + ": " + str(e))
    return errorMessages


"""

ON MESSAGE

"""

@client.event
async def on_message(message):

#Bot Ping
    if client.user.id in message.content:
        try:
            await client.cogs["ping"].pingbot(message)
        except:
            pass


#Ignore all bots
    if message.author.bot:
        return

#Prepare Message for Commands (make it lowercase)
    mesBox = message.content.split(" ")
    mesBox[0] = mesBox[0].lower()
    message.content = " ".join(mesBox)
    await client.process_commands(message)



def run_client(client, *args, **kwargs):
    loop = asyncio.get_event_loop()
    while True:
        try:
            loop.run_until_complete(client.start(*args, **kwargs))
        except Exception as e:
            print("Error", e)  # or use proper logging
        print("Waiting until restart")
        time.sleep(600)

run_client(client, "NDE0NDIxNzkwOTU0MzU2NzU2.DXDDbQ.BG89ffZ64dacnmSOpoufvIJ3Zoo")
#client.run("NDE0NDIxNzkwOTU0MzU2NzU2.DXDDbQ.BG89ffZ64dacnmSOpoufvIJ3Zoo")
